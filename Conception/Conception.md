# Conception de l'application
## Liste des cas d'usage
- Afficher la liste des paris à venir
- Créer un compte
- Se connecter
- Déposer/retirer des fonds
- Poser un pari choisi par sport
- Poser un pari choisi par évènement
- Poser un pari choisi par type pari
- Consulter /modifier le profil / les documents
- Consulter l'historique des paris / paiements
- Modifier le panier (modifier paris)
- Supression du compte

## Cas d'usage 1 : Afficher les paris
### Flux de travail utilisateur

```mermaid
graph LR
	Accueil( ) --'Ouvrir l'application'--> connect(Accueil)  
```
### Diagramme de séquence :

```mermaid
sequenceDiagram
    participant Mobile
    participant API
	participant SQL
	Mobile ->> API: requête 
	API ->> SQL: recherche des évènements à venir
	SQL ->> API: réception des évènements à venir
	API ->> Mobile: affichage de la réponse
```


## Cas d'usage 2 : Créer un compte
### Flux de travail utilisateur

```mermaid
graph LR
	creationDeCompte(Création de compte) -- saisie des informations --> remplissageChamps{Succès ?}
	remplissageChamps -- oui --> verificationParLien
	remplissageChamps -- non --> creationDeCompte(Création de compte)
	verificationParLien --> Accueil(Affichage paris)
```

### Diagramme de séquence :

Séquences MVC
```mermaid
sequenceDiagram
    participant Mobile
    participant API
	participant SQL
	Mobile ->> API: envoie les infos utilisateur des champs
	API ->> API: vérifie les données reçues
	API ->> SQL: inscrit les données en base
	API ->> Mobile: affichage de la réponse
```
<!-- note : manque la validation par lien (controller web) -->


## Cas d'usage 3 : Se connecter

```mermaid
graph LR
	Accueil(Accueil) --'Se connecter'--> connect(Connexion)  
	connect -- saisie --> login{Succès ?}
	login -- non --> connect
	login -- oui --> profile(Profile)
```
### Diagramme de séquence :

Séquences MVC
```mermaid
sequenceDiagram
    participant Mobile
    participant API
	participant SQL
	Mobile ->> API: connect(user, pass)
	API ->> SQL: get_user_info(user)
	SQL ->> API: user_info
	API ->> API: check_credentials
	API ->> Mobile: response
```


## Cas d'usage 4 : Déposer / retirer des fonds

```mermaid
graph LR
	Wallet(Wallet) --'Ajouter/retirer des fonds'--> ajoutRetraitFonds(Ajout / retrait de fonds)  
	ajoutRetraitFonds -- saisie --> paiement{Succès ?}
	paiement -- non --> ajoutRetraitFonds
	paiement -- oui --> Wallet(Wallet)
```
### Diagramme de séquence :

Séquences MVC
```mermaid
sequenceDiagram
    participant Mobile
    participant API
	participant SQL
	Mobile ->> API: envoi du montant à modifier
	API ->> API: vérifie les données reçues
	API ->> SQL: modification du champ wallet
	SQL ->> API: renvoie le contenu du champ wallet
	API ->> Mobile: affichage de la réponse
```


## Cas d'usage 5 : Poser un pari choisi par sport

```mermaid
graph LR
	Accueil(Accueil) --'Sélection du sport'--> pageSport(Sport) 
	pageSport(Page sport) --'Sélection du pari'--> ajoutMontant(Montant)  
	ajoutMontant -- saisie --> paiement{Succès ?}
	paiement -- non --> ajoutMontant
	paiement -- oui --> validation(Validation)
	validation -- non --> ajoutMontant(Montant)
	validation -- oui --> Accueil(Accueil)
```
### Diagramme de séquence :

Séquences MVC
```mermaid
sequenceDiagram
    participant Mobile
    participant API
	participant SQL
	Mobile ->> API: envoi des informations du pari
	API ->> API: vérifie les données reçues
	API ->> SQL: inscription du pari en base
	API ->> Mobile: affichage de la réponse (alert)
```


## Cas d'usage 6 : Poser un pari choisi par évènement

```mermaid
graph LR
	Accueil(Accueil) --'Sélection de l'évènement'--> pageEvenement(Evenement) 
	pageEvenement(Evenement) --'Sélection du pari'--> ajoutMontant(Montant)  
	ajoutMontant -- saisie --> paiement{Succès ?}
	paiement -- non --> ajoutMontant
	paiement -- oui --> validation(Validation)
	validation -- non --> ajoutMontant(Montant)
	validation -- oui --> Accueil(Accueil)
```
### Diagramme de séquence :

Séquences MVC
```mermaid
sequenceDiagram
    participant Mobile
    participant API
	participant SQL
	Mobile ->> API: envoi des informations du pari
	API ->> API: vérifie les données reçues
	API ->> SQL: inscription du pari en base
	API ->> Mobile: affichage de la réponse (alert)
```


## Cas d'usage 7 : Poser un pari choisi par type de pari

```mermaid
graph LR
	Accueil(Accueil) --'Sélection de l'évènement'--> pageEvenement(Evenement) 
	pageEvenement(Evenement) --'Sélection du type de pari'--> choixType(Type)  
	choixType(Type) --'Sélection du pari'--> ajoutMontant(Montant)  
	ajoutMontant -- saisie --> paiement{Succès ?}
	paiement -- non --> ajoutMontant
	paiement -- oui --> validation(Validation)
	validation -- non --> ajoutMontant( Montant)
	validation -- oui --> Accueil(Accueil)
```
### Diagramme de séquence :

Séquences MVC
```mermaid
sequenceDiagram
    participant Mobile
    participant API
	participant SQL
	Mobile ->> API: envoi des informations du pari
	API ->> API: vérifie les données reçues
	API ->> SQL: inscription du pari en base
	API ->> Mobile: affichage de la réponse (alert)
```


## Cas d'usage 8 : Consulter ou modifier le profil ou les documents utilisateur

```mermaid
graph LR
	Profil(Profil) --'Éditer les infos'--> editerInfos(Édition)  
	editerInfos -- saisie --> confirmation{Confirmation ?}
	confirmation -- non --> editerInfos
	confirmation -- oui --> Profil(Profil)
```
### Diagramme de séquence :

Séquences MVC
```mermaid
sequenceDiagram
    participant Mobile
    participant API
	participant SQL
	Mobile ->> API: envoi des informations à modifier
	API ->> API: vérifie les données reçues
	API ->> SQL: modification des champs du profil
	SQL ->> API: renvoie le contenu des champs
	API ->> Mobile: affichage de la réponse
```


## Cas d'usage 9 : Consulter l'historique des paris

```mermaid
graph LR
	Profil(Profil) --'consulter l'historique'--> historique(Historique)  
```
### Diagramme de séquence :

Séquences MVC
```mermaid
sequenceDiagram
    participant Mobile
    participant API
	participant SQL
	Mobile ->> API: envoi de la requête
	API ->> SQL: interroge la base de données
	SQL ->> API: renvoie les données
	API ->> Mobile: affichage de la réponse
```



## Cas d'usage 10 : Modifier / annuler le panier / un pari en cours

```mermaid
graph LR
	Panier(Paris en cours) --'Modifier / annuler un pari'--> modificationPari(Modifier / annuler le pari)
	modificationPari -- saisie --> confirmation{Confirmation ?}
	confirmation -- non --> modificationPari
	confirmation -- oui --> Panier(Paris en cours)
```
### Diagramme de séquence :

Séquences MVC
```mermaid
sequenceDiagram
    participant Mobile
    participant API
	participant SQL
	Mobile ->> API: envoi des informations à modifier
	API ->> API: vérifie les données reçues
	API ->> SQL: modification / supression du pari
	SQL ->> API: renvoie le contenu des paris
	API ->> Mobile: affichage de la réponse
```



## Cas d'usage 11 : Supprimer le compte

```mermaid
graph LR
	Profil(Profil) --'Supprimer le compte'--> suppressionCompte(Suppression)  
	suppressionCompte --> confirmation{Confirmation ?}
	confirmation -- non --> Profil
	confirmation -- oui --> Accueil(Accueil)
```
### Diagramme de séquence :

Séquences MVC
```mermaid
sequenceDiagram
    participant Mobile
    participant API
	participant SQL
	Mobile ->> API: envoi de la requête
	API ->> SQL: retrait des données de la base
	API ->> Mobile: affichage de la réponse
```

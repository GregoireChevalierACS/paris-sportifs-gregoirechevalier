DROP TABLE IF EXISTS bet;
CREATE TABLE bet (
  id INT AUTO_INCREMENT NOT NULL,
  evenement_id INT NOT NULL,
  name_bet VARCHAR(255) NOT NULL,
  cote INT NOT NULL,
  date_bet_limit DATETIME NOT NULL,
  result_bet TINYINT(1) DEFAULT NULL,
  PRIMARY KEY(id)
);
DROP TABLE IF EXISTS bet_user;
CREATE TABLE bet_user (
  id INT AUTO_INCREMENT NOT NULL,
  user_id INT DEFAULT NULL,
  bet_id INT DEFAULT NULL,
  amount_bet_date DATETIME NOT NULL,
  amount_bet INT DEFAULT NULL,
  gain_possible INT DEFAULT NULL,
  PRIMARY KEY(id)
);
DROP TABLE IF EXISTS competition;
CREATE TABLE competition (
  id INT AUTO_INCREMENT NOT NULL,
  sport_id INT DEFAULT NULL,
  name VARCHAR(255) NOT NULL,
  start_at DATETIME NOT NULL,
  end_at DATETIME NOT NULL,
  PRIMARY KEY(id)
);
DROP TABLE IF EXISTS document_user;
CREATE TABLE document_user (
  id INT AUTO_INCREMENT NOT NULL,
  brochure_filename VARCHAR(255) NOT NULL,
  is_valid TINYINT(1) NOT NULL,
  PRIMARY KEY(id)
);
DROP TABLE IF EXISTS equipe;
CREATE TABLE equipe (
  id INT AUTO_INCREMENT NOT NULL,
  sport_id INT DEFAULT NULL,
  evenement_sport_id INT DEFAULT NULL,
  name VARCHAR(255) NOT NULL,
  contry VARCHAR(255) NOT NULL,
  PRIMARY KEY(id)
);
DROP TABLE IF EXISTS evenement_sport;
CREATE TABLE evenement_sport (
  id INT AUTO_INCREMENT NOT NULL,
  sport_id INT DEFAULT NULL,
  competionn_id INT DEFAULT NULL,
  name VARCHAR(255) NOT NULL,
  begin_date DATETIME NOT NULL,
  event_place VARCHAR(255) NOT NULL,
  PRIMARY KEY(id)
);
DROP TABLE IF EXISTS joueurs;
CREATE TABLE joueurs (
  id INT AUTO_INCREMENT NOT NULL,
  equipe_id INT DEFAULT NULL,
  sport_id INT DEFAULT NULL,
  name VARCHAR(255) NOT NULL,
  lastname VARCHAR(255) NOT NULL,
  status VARCHAR(255) NOT NULL,
  PRIMARY KEY(id)
);
DROP TABLE IF EXISTS reset_password_request;
CREATE TABLE reset_password_request (
  id INT AUTO_INCREMENT NOT NULL,
  user_id INT NOT NULL,
  selector VARCHAR(20) NOT NULL,
  hashed_token VARCHAR(100) NOT NULL,
  requested_at DATETIME NOT NULL COMMENT '(DC2Type:datetime_immutable)',
  expires_at DATETIME NOT NULL COMMENT '(DC2Type:datetime_immutable)',
  PRIMARY KEY(id)
);
DROP TABLE IF EXISTS sport;
CREATE TABLE sport (
  id INT AUTO_INCREMENT NOT NULL,
  name VARCHAR(255) NOT NULL,
  nb_teams INT NOT NULL,
  nb_players INT NOT NULL,
  PRIMARY KEY(id)
);
DROP TABLE IF EXISTS user;
CREATE TABLE user (
  id INT AUTO_INCREMENT NOT NULL,
  wallet_id INT NOT NULL,
  document_id INT NOT NULL,
  firstname VARCHAR(255) NOT NULL,
  lastname VARCHAR(255) NOT NULL,
  email VARCHAR(180) NOT NULL,
  birth_date DATETIME NOT NULL,
  street VARCHAR(255) NOT NULL,
  street_number VARCHAR(255) DEFAULT NULL,
  code_postal VARCHAR(5) NOT NULL,
  city VARCHAR(180) NOT NULL,
  phone VARCHAR(255) NOT NULL,
  create_date DATETIME NOT NULL,
  user_validation TINYINT(1) NOT NULL,
  user_verified TINYINT(1) NOT NULL,
  user_validation_date DATETIME DEFAULT NULL,
  user_suspended TINYINT(1) NOT NULL,
  user_suspended_date DATETIME DEFAULT NULL,
  user_deleted TINYINT(1) NOT NULL,
  user_deleted_date DATETIME DEFAULT NULL,
  roles LONGTEXT NOT NULL COMMENT '(DC2Type:json)',
  password VARCHAR(255) NOT NULL,
  PRIMARY KEY(id)
);
DROP TABLE IF EXISTS wallet;
CREATE TABLE wallet (
  id INT AUTO_INCREMENT NOT NULL,
  credit INT NOT NULL,
  PRIMARY KEY(id)
);
INSERT INTO `bet` (`id`, `evenement_id`, `name_bet`, `cote`, `date_bet_limit`, `result_bet`) VALUES
(1, 1, 'victoire Barcelone', 120, '2021-03-03 14:56:45', 0),
(2, 1, 'victoire Liverpool', 220, '2021-03-03 14:56:45', 0),
(3, 1, 'match null', 300, '2021-03-03 14:56:45', 0),
(4, 2, 'victoire nice', 220, '2021-03-03 14:56:45', 0),
(5, 2, 'victoire Strasbourg', 120, '2021-03-03 14:56:45', 0),
(6, 2, 'match null', 500, '2021-03-03 14:56:45', 0);
INSERT INTO `competition` (`id`, `sport_id`, `name`, `start_at`, `end_at`) VALUES
(1, 1, 'champions ligue', '2021-01-29 19:00:00', '2021-01-29 21:00:00'),
(2, 2, 'championnat handball', '2021-01-29 19:00:00', '2021-01-29 21:00:00');
INSERT INTO `document_user` (`id`, `brochure_filename`, `is_valid`) VALUES
(1, ' ', 0),
(2, ' ', 0),
(3, ' ', 0);
INSERT INTO `equipe` (`id`, `sport_id`, `evenement_sport_id`, `name`, `contry`) VALUES
(1, 1, 1, 'Liverpool', 'Anglettere'),
(2, 1, 1, 'barcelonne', 'Espagne'),
(3, 2, 2, 'Nice', 'France'),
(4, 2, 2, 'Strasbourg', 'France');
INSERT INTO `evenement_sport` (`id`, `sport_id`, `competionn_id`, `name`, `begin_date`, `event_place`) VALUES
(1, 1, 1, 'Phase de groupes - journée 2', '2021-01-29 19:00:00', 'Espagne'),
(2, 2, 2, 'Premier tour - journée 1', '2021-01-29 19:00:00', 'France');
INSERT INTO `joueurs` (`id`, `equipe_id`, `sport_id`, `name`, `lastname`, `status`) VALUES
(1, 2, 1, 'Stegen', 'Marc', 'Titulaire'),
(2, 2, 1, 'Gerrard', 'Pique', 'Titulaire'),
(3, 2, 1, 'Sergino', 'Dest', 'Titulaire'),
(4, 2, 1, 'Clement', 'Lenglet', 'Titulaire'),
(5, 2, 1, 'Jordi', 'Alba', 'Titulaire'),
(6, 2, 1, 'Sergio', 'Busquets', 'Titulaire'),
(7, 2, 1, 'Miralem', 'Pjanic', 'Titulaire'),
(8, 2, 1, 'Philippe', 'Coutinho', 'Titulaire'),
(9, 2, 1, 'Antonie', 'Griezmann', 'Titulaire'),
(10, 2, 1, 'Lionel', 'Messi', 'Titulaire'),
(11, 2, 1, 'Ansu', 'Fati', 'Titulaire'),
(12, 1, 1, 'Alisson', 'Becker', 'Titulaire'),
(13, 1, 1, 'Robertson', 'Andrew', 'Titulaire'),
(14, 1, 1, 'Henderson', 'Jordan', 'Titulaire'),
(15, 1, 1, 'Fabinho', 'Henrique', 'Titulaire'),
(16, 1, 1, 'AlexanderArnold', 'Trent', 'Titulaire'),
(17, 1, 1, 'Wijnaldum', 'Georginio', 'Titulaire'),
(18, 1, 1, 'Alcantara', 'Thiago', 'Titulaire'),
(19, 1, 1, 'Shaqiri', 'Wherdan', 'Titulaire'),
(20, 1, 1, 'Mane', 'Roberto', 'Titulaire'),
(21, 1, 1, 'Firmino', 'Sadio', 'Titulaire'),
(22, 1, 1, 'Salah', 'Mohammed', 'Titulaire'),
(23, 3, 2, 'Marija', 'Colic', 'Titulaire'),
(24, 3, 2, 'Kimberley', 'Boucharb', 'Titulaire'),
(25, 3, 2, 'Marie', 'Prouvensier', 'Titulaire'),
(26, 3, 2, 'Marija', 'Janjic', 'Titulaire'),
(27, 3, 2, 'Martina', 'Skolkova', 'Titulaire'),
(28, 3, 2, 'Melissa', 'Agathe', 'Titulaire'),
(29, 3, 2, 'Ehsan', 'Abdelmalek', 'Titulaire'),
(30, 4, 2, 'Romain', 'Marhias', 'Titulaire'),
(31, 4, 2, 'Maxime', 'Duchene', 'Titulaire'),
(32, 4, 2, 'Jimmy', 'Portes', 'Titulaire'),
(33, 4, 2, 'Yvan', 'Gerard', 'Titulaire'),
(34, 4, 2, 'Lucien', 'Auffret', 'Titulaire'),
(35, 4, 2, 'Lewis', 'Anzuini', 'Titulaire'),
(36, 4, 2, 'Maxime', 'Mika', 'Titulaire');
INSERT INTO `sport` (`id`, `name`, `nb_teams`, `nb_players`) VALUES
(1, 'football', 2, 11),
(2, 'handball', 2, 7);
INSERT INTO `user` (`id`, `wallet_id`, `document_id`, `firstname`, `lastname`, `email`, `birth_date`, `street`, `street_number`, `code_postal`, `city`, `phone`, `create_date`, `user_validation`, `user_verified`, `user_validation_date`, `user_suspended`, `user_suspended_date`, `user_deleted`, `user_deleted_date`, `roles`, `password`) VALUES
(1, 1, 1, 'namoune', 'Sofiane', 'sofiane1@gmail.com', '1994-01-04 14:56:43', 'Rue vauban', '85', '68100', 'Mulhouse', '0797478532', '2021-03-01 14:56:43', 0, 0, NULL, 0, NULL, 0, NULL, '[\"ROLE_USER\"]', '$2y$12$2ccpBT1toKcy/1GWcEcNbeeV2Bp6jjnh.T4ia49tfe6HmyPbpUM0W'),
(2, 2, 2, 'bazine', 'mohammmed', 'sofiane2@gmail.com', '1994-01-04 14:56:43', 'XX xx', '70', '75000', 'paris', '0697478532', '2021-03-01 14:56:43', 0, 0, NULL, 0, NULL, 0, NULL, '[\"ROLE_USER\"]', '$2y$12$2ccpBT1toKcy/1GWcEcNbeeV2Bp6jjnh.T4ia49tfe6HmyPbpUM0W'),
(3, 3, 3, 'Tintin', 'Dupont', 'tintin.dupont@mail.com', '1994-02-21 14:56:43', 'Rue de la bichette', '3', '67640', 'Lipsheim', '0612345678', '2021-03-01 14:56:43', 0, 0, NULL, 0, NULL, 0, NULL, '[\"ROLE_ADMIN\"]', '$2y$13$U/fXrYeOGKHTj.WV4jVUJ.jyORDnwfPPUhqMzSC7RxipiKwRoAxV6');
INSERT INTO `wallet` (`id`, `credit`) VALUES
(1, 0),
(2, 0),
(3, 3000);

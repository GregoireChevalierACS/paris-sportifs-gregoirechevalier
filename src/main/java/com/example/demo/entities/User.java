package com.example.demo.entities;

import javax.persistence.*;
import java.util.Date;

@Entity
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    int id;

    @Column(name = "wallet_id")
    private int walletID;
    @Column(name = "document_id")
    private int documentId;

    private String firstname;
    private String lastname;
    private String email;

    @Temporal(value = TemporalType.DATE)
    private Date birth_date;
    private String street;
    private int street_number;
    private int code_postal;
    private String city;
    private int phone;

    @Temporal(value = TemporalType.DATE)
    private Date create_date;
    private boolean user_validation;
    private boolean user_verified;
    @Temporal(value = TemporalType.TIMESTAMP)
    private Date user_validation_date;
    private boolean user_suspended;
    @Temporal(value = TemporalType.TIMESTAMP)
    private Date user_suspended_date;
    private boolean user_deleted;
    @Temporal(value = TemporalType.TIMESTAMP)
    private Date user_deleted_date;

    private String roles;
    private String password;


    public int getWalletID() {
        return walletID;
    }

    public void setWalletID(int walletID) {
        this.walletID = walletID;
    }

    public int getdocumentId() {
        return documentId;
    }

    public void setdocumentId(int documentId) {
        this.documentId = documentId;
    }

    public String getfirstname() {
        return firstname;
    }

    public void setfirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getlastname() {
        return lastname;
    }

    public void setlastname(String lastname) {
        this.lastname = lastname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getbirth_date() {
        return birth_date;
    }

    public void setbirth_date(Date birth_date) {
        this.birth_date = birth_date;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public int getstreet_number() {
        return street_number;
    }

    public void setstreet_number(int street_number) {
        this.street_number = street_number;
    }

    public int getcode_postal() {
        return code_postal;
    }

    public void setcode_postal(int code_postal) {
        this.code_postal = code_postal;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public int getphone() {
        return phone;
    }

    public void setphone(int phone) {
        this.phone = phone;
    }

    public Date getCreate_date() {
        return create_date;
    }

    public void setCreate_date(Date create_date) {
        this.create_date = create_date;
    }

    public boolean isuser_validation() {
        return user_validation;
    }

    public void setuser_validation(boolean user_validation) {
        this.user_validation = user_validation;
    }

    public boolean isuser_verified() {
        return user_verified;
    }

    public void setuser_verified(boolean user_verified) {
        this.user_verified = user_verified;
    }

    public Date getuser_validation_date() {
        return user_validation_date;
    }

    public void setuser_validation_date(Date user_validation_date) {
        this.user_validation_date = user_validation_date;
    }

    public boolean isuser_suspended() {
        return user_suspended;
    }

    public void setuser_suspended(boolean user_suspended) {
        this.user_suspended = user_suspended;
    }

    public Date getuser_suspended_date() {
        return user_suspended_date;
    }

    public void setuser_suspended_date(Date user_suspended_date) {
        this.user_suspended_date = user_suspended_date;
    }

    public boolean isuser_deleted() {
        return user_deleted;
    }

    public void setuser_deleted(boolean user_deleted) {
        this.user_deleted = user_deleted;
    }

    public Date getuser_deleted_date() {
        return user_deleted_date;
    }

    public void setuser_deleted_date(Date user_deleted_date) {
        this.user_deleted_date = user_deleted_date;
    }

    public String getroles() {
        return roles;
    }

    public void setroles(String roles) {
        this.roles = roles;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}

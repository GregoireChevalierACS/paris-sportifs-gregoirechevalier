package com.example.demo.entities;

import org.springframework.data.repository.CrudRepository;

public interface ResetPasswordRequestRepository extends CrudRepository<ResetPasswordRequest, Integer> {
}

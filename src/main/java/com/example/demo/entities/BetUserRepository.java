package com.example.demo.entities;

import org.springframework.data.repository.CrudRepository;

public interface BetUserRepository extends CrudRepository<BetUser, Integer> {
}

package com.example.demo.entities;

import javax.persistence.*;
import java.util.Date;

@Entity
public class ResetPasswordRequest {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    int id;

    private int user_id;
    private String selector;
    private String hashed_token;

    @Temporal(value = TemporalType.DATE)
    private Date requested_at;
    @Temporal(value = TemporalType.DATE)
    private Date expires_at;

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getSelector() {
        return selector;
    }

    public void setSelector(String selector) {
        this.selector = selector;
    }

    public String getHashed_token() {
        return hashed_token;
    }

    public void setHashed_token(String hashed_token) {
        this.hashed_token = hashed_token;
    }

    public Date getRequested_at() {
        return requested_at;
    }

    public void setRequested_at(Date requested_at) {
        this.requested_at = requested_at;
    }

    public Date getExpires_at() {
        return expires_at;
    }

    public void setExpires_at(Date expires_at) {
        this.expires_at = expires_at;
    }


}

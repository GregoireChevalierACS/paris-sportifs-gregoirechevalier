package com.example.demo.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class DocumentUser {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    int id;

    private String brochure_filename;
    private boolean is_valid;

    public String getBrochure_filename() {
        return brochure_filename;
    }

    public void setBrochure_filename(String brochure_filename) {
        this.brochure_filename = brochure_filename;
    }

    public boolean isIs_valid() {
        return is_valid;
    }

    public void setIs_valid(boolean is_valid) {
        this.is_valid = is_valid;
    }


}

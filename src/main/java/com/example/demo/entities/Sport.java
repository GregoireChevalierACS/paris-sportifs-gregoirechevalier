package com.example.demo.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Sport {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    int id;

    private int nb_teams;

    private int nb_players;

    public int getNb_teams() {
        return nb_teams;
    }

    public void setNb_teams(int nb_teams) {
        this.nb_teams = nb_teams;
    }

    public int getNb_players() {
        return nb_players;
    }

    public void setNb_players(int nb_players) {
        this.nb_players = nb_players;
    }

}

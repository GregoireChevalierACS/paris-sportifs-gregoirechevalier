package com.example.demo.entities;

import org.springframework.data.repository.CrudRepository;

public interface SportRepository extends CrudRepository<Sport, Integer> {
}

package com.example.demo.entities;

import javax.persistence.*;
import java.util.Date;

@Entity
public class Bet {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    int id;

    private int evenement_id;
    private String name_bet;
    private int cote;
    @Temporal(value = TemporalType.DATE)
    private Date date_bet_limit;
    private boolean result_bet;

    public int getEvenement_id() {
        return evenement_id;
    }

    public void setEvenement_id(int evenement_id) {
        this.evenement_id = evenement_id;
    }

    public String getName_bet() {
        return name_bet;
    }

    public void setName_bet(String name_bet) {
        this.name_bet = name_bet;
    }

    public int getCote() {
        return cote;
    }

    public void setCote(int cote) {
        this.cote = cote;
    }

    public Date getDate_bet_limit() {
        return date_bet_limit;
    }

    public void setDate_bet_limit(Date date_bet_limit) {
        this.date_bet_limit = date_bet_limit;
    }

    public boolean isResult_bet() {
        return result_bet;
    }

    public void setResult_bet(boolean result_bet) {
        this.result_bet = result_bet;
    }


}

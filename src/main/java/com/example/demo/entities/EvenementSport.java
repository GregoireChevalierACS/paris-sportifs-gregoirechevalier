package com.example.demo.entities;

import javax.persistence.*;
import java.util.Date;

@Entity
public class EvenementSport {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    int id;

    private int sport_id;
    private int competionn_id;
    private String name;

    @Temporal(value = TemporalType.DATE)
    private Date begin_date;

    private String event_place;

    public int getSport_id() {
        return sport_id;
    }

    public void setSport_id(int sport_id) {
        this.sport_id = sport_id;
    }

    public int getCompetionn_id() {
        return competionn_id;
    }

    public void setCompetionn_id(int competionn_id) {
        this.competionn_id = competionn_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getBegin_date() {
        return begin_date;
    }

    public void setBegin_date(Date begin_date) {
        this.begin_date = begin_date;
    }

    public String getEvent_place() {
        return event_place;
    }

    public void setEvent_place(String event_place) {
        this.event_place = event_place;
    }


}

package com.example.demo.entities;

import javax.persistence.*;
import java.util.Date;

@Entity
public class BetUser {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    int id;

    private int user_id;
    private int bet_id;
    @Temporal(value = TemporalType.DATE)
    private Date amount_bet_date;
    private int amount_bet;
    private int gain_possible;

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public int getBet_id() {
        return bet_id;
    }

    public void setBet_id(int bet_id) {
        this.bet_id = bet_id;
    }

    public Date getAmount_bet_date() {
        return amount_bet_date;
    }

    public void setAmount_bet_date(Date amount_bet_date) {
        this.amount_bet_date = amount_bet_date;
    }

    public int getAmount_bet() {
        return amount_bet;
    }

    public void setAmount_bet(int amount_bet) {
        this.amount_bet = amount_bet;
    }

    public int getGain_possible() {
        return gain_possible;
    }

    public void setGain_possible(int gain_possible) {
        this.gain_possible = gain_possible;
    }


}

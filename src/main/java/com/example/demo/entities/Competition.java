package com.example.demo.entities;

import javax.persistence.*;
import java.util.Date;

@Entity
public class Competition {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    int id;

    private int sport_id;
    private String name;
    @Temporal(value = TemporalType.DATE)
    private Date start_at;
    @Temporal(value = TemporalType.DATE)
    private Date end_at;

    public int getSport_id() {
        return sport_id;
    }

    public void setSport_id(int sport_id) {
        this.sport_id = sport_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getStart_at() {
        return start_at;
    }

    public void setStart_at(Date start_at) {
        this.start_at = start_at;
    }

    public Date getEnd_at() {
        return end_at;
    }

    public void setEnd_at(Date end_at) {
        this.end_at = end_at;
    }



}

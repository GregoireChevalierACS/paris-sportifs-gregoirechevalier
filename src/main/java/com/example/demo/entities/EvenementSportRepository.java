package com.example.demo.entities;

import org.springframework.data.repository.CrudRepository;

public interface EvenementSportRepository extends CrudRepository<EvenementSport, Integer> {
}

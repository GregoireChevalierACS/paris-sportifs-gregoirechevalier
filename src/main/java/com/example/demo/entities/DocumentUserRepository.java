package com.example.demo.entities;

import org.springframework.data.repository.CrudRepository;

public interface DocumentUserRepository extends CrudRepository<DocumentUser, Integer> {
}

package com.example.demo.entities;

import org.springframework.data.repository.CrudRepository;

public interface EquipeRepository extends CrudRepository<Equipe, Integer> {
}

package com.example.demo.entities;

import org.springframework.data.repository.CrudRepository;

public interface JoueursRepository extends CrudRepository<Joueurs, Integer> {
}

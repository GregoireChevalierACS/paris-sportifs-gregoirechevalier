package com.example.demo.entities;

import org.springframework.data.repository.CrudRepository;

public interface BetRepository extends CrudRepository<Bet, Integer> {
}

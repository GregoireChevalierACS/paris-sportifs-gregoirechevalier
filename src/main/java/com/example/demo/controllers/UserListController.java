package com.example.demo.controllers;

import com.example.demo.entities.User;
import com.example.demo.entities.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserListController {

    @Autowired
    private UserRepo userRepo;

    @GetMapping("/users")
    public Iterable<User> showUsersList(){
        return userRepo.findAll();
    }

}
